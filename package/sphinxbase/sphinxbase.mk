#############################################################
#
# sphinxbase
#
#############################################################

SPHINXBASE_VERSION = 0.7
SPHINXBASE_SOURCE =sphinxbase-$(SPHINXBASE_VERSION).tar.gz
SPHINXBASE_SITE = http://ignum.dl.sourceforge.net/project/cmusphinx/sphinxbase/$(SPHINXBASE_VERSION)/
SPHINXBASE_AUTORECONF = YES
SPHINXBASE_INSTALL_STAGING = YES
SPHINXBASE_INSTALL_STAGING_OPT = DESTDIR=$(STAGING_DIR) LDFLAGS=-L$(STAGING_DIR)/usr/lib install

$(eval $(call AUTOTARGETS))
