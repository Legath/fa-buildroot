
FESTIVAL_VERSION=2.1
FESTIVAL_SOURCE=festival-$(FESTIVAL_VERSION)-release.tar.gz
FESTIVAL_SITE=http://www.cstr.ed.ac.uk/downloads/festival/$(FESTIVAL_VERSION)/
FESTIVAL_DEPENDENCIES+= speech-tools

#FESTIVAL_CONF_OPT+="EST=$(SPEECH_TOOLS_DIR)"
define FESTIVAL_CONFIGURE_CMDS
	@$(SED) 's/BUILD_DIRS = src lib examples bin doc/BUILD_DIRS = src lib examples bin/g' $(@D)/Makefile
	cd $(@D); CC="$(TARGET_CC)" AR=$(TARGET_AR) RANLIB=$(TARGET_RANLIB) EST="$(SPEECH_TOOLS_DIR)" ./configure --target=arm-unknown-linux-gnueabi --host=arm-unknown-linux-gnueabi --build=x86_64-unknown-linux-gnu --disable-doc --without-doc
	
endef

define FESTIVAL_BUILD_CMDS
	$(MAKE1) CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" EST="$(SPEECH_TOOLS_DIR)" -C $(@D)
	$(MAKE1) CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" EST="$(SPEECH_TOOLS_DIR)" -C $(@D)/src/modules init_modules.cc

endef

$(eval $(call AUTOTARGETS))
