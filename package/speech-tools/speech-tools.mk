


SPEECH_TOOLS_VERSION=2.1
SPEECH_TOOLS_SOURCE=speech_tools-$(SPEECH_TOOLS_VERSION)-release.tar.gz
SPEECH_TOOLS_SITE=http://www.cstr.ed.ac.uk/downloads/festival/$(SPEECH_TOOLS_VERSION)/
SPEECH_TOOLS_DEPENDENCIES = ncurses


define SPEECH_TOOLS_CONFIGURE_CMDS
	cd $(@D);./configure CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" AR=$(TARGET_AR) RANLIB=$(TARGET_RANLIB) --host=arm-unknown-linux-gnueabi --target=arm-unknown-linux-gnueabi 
	
endef

define SPEECH_TOOLS_BUILD_CMDS
	$(MAKE1) CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" -C $(@D)
	(ln -s $(@D) $(BUILD_DIR)/speech_tools || true)

endef

$(eval $(call AUTOTARGETS))
