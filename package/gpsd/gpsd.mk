#############################################################
#
# gpsd
#
#############################################################

GPSD_VERSION = 3.6
GPSD_SITE = http://download.savannah.gnu.org/releases/gpsd/
GPSD_INSTALL_STAGING = YES
GPSD_DEPENDENCIES+=host-scons host-pkg-config

# Build libgpsmm if we've got C++
ifeq ($(BR2_INSTALL_LIBSTDCPP),y)
FEATURES+= 'libgpsmm=true'
else
FEATURES+= 'libgpsmm=false'
endif

# Enable or disable Qt binding
ifeq ($(BR2_PACKAGE_QT_NETWORK),y)
FEATURES+='libQgpsmm=true'
GPSD_DEPENDENCIES += qt 
else
FEATURES+='libQgpsmm=false'
endif

# If libusb is available build it before so the package can use it
ifeq ($(BR2_PACKAGE_LIBUSB),y)
FEATURES+='usb=true'
GPSD_DEPENDENCIES += libusb
endif

ifeq ($(strip $(BR2_PACKAGE_DBUS)),y)
FEATURES+='dbus_export=true'
GPSD_DEPENDENCIES += dbus dbus-glib
endif

ifeq ($(BR2_PACKAGE_NCURSES),y)
GPSD_DEPENDENCIES += ncurses
endif

# Protocol support
ifneq ($(BR2_PACKAGE_GPSD_ASHTECH),y)
FEATURES+='ashtech=false'
endif
ifneq ($(BR2_PACKAGE_GPSD_AIVDM),y)
FEATURES+='aivdm=false'
endif
ifneq ($(BR2_PACKAGE_GPSD_EARTHMATE),y)
FEATURES+='earthmate=false'
endif
ifneq ($(BR2_PACKAGE_GPSD_EVERMORE),y)
FEATURES+='evermore=false'
endif
ifneq ($(BR2_PACKAGE_GPSD_FV18),y)
FEATURES+='fv18=false'
endif
ifneq ($(BR2_PACKAGE_GPSD_GARMIN),y)
FEATURES+='garmin=false'
endif
ifneq ($(BR2_PACKAGE_GPSD_GARMIN_SIMPLE_TXT),y)
FEATURES+='garmintxt=false'
endif
ifneq ($(BR2_PACKAGE_GPSD_GPSCLOCK),y)
FEATURES+='gpsclock=false'
endif
ifneq ($(BR2_PACKAGE_GPSD_ITRAX),y)
	GPSD_CONF_OPT += --disable-itrax
endif
ifneq ($(BR2_PACKAGE_GPSD_MTK3301),y)
	GPSD_CONF_OPT += --disable-mtk3301
endif
ifneq ($(BR2_PACKAGE_GPSD_NMEA),y)
	GPSD_CONF_OPT += --disable-nmea
endif
ifneq ($(BR2_PACKAGE_GPSD_NTRIP),y)
	GPSD_CONF_OPT += --disable-ntrip
endif
ifneq ($(BR2_PACKAGE_GPSD_NAVCOM),y)
	GPSD_CONF_OPT += --disable-navcom
endif
ifneq ($(BR2_PACKAGE_GPSD_OCEANSERVER),y)
	GPSD_CONF_OPT += --disable-oceanserver
endif
ifneq ($(BR2_PACKAGE_GPSD_ONCORE),y)
	GPSD_CONF_OPT += --disable-oncore
endif
ifneq ($(BR2_PACKAGE_GPSD_RTCM104V2),y)
	GPSD_CONF_OPT += --disable-rtcm104v2
endif
ifneq ($(BR2_PACKAGE_GPSD_RTCM104V3),y)
	GPSD_CONF_OPT += --disable-rtcm104v3
endif
ifneq ($(BR2_PACKAGE_GPSD_SIRF),y)
	GPSD_CONF_OPT += --disable-sirf
endif
ifneq ($(BR2_PACKAGE_GPSD_SUPERSTAR2),y)
	GPSD_CONF_OPT += --disable-superstar2
endif
ifneq ($(BR2_PACKAGE_GPSD_TRIMBLE_TSIP),y)
	GPSD_CONF_OPT += --disable-tsip
endif
ifneq ($(BR2_PACKAGE_GPSD_TRIPMATE),y)
	GPSD_CONF_OPT += --disable-tripmate
endif
ifeq ($(BR2_PACKAGE_GPSD_TRUE_NORTH),y)
	GPSD_CONF_OPT += --enable-tnt
endif
ifneq ($(BR2_PACKAGE_GPSD_UBX),y)
	GPSD_CONF_OPT += --disable-ubx
endif

# Features
ifneq ($(BR2_PACKAGE_GPSD_NTP_SHM),y)
	GPSD_CONF_OPT += --disable-ntpshm
endif
ifneq ($(BR2_PACKAGE_GPSD_PPS),y)
	GPSD_CONF_OPT += --disable-pps
endif
ifeq ($(BR2_PACKAGE_GPSD_PPS_ON_CTS),y)
	GPSD_CONF_OPT += --enable-pps-on-cts
endif
ifeq ($(BR2_PACKAGE_GPSD_SQUELCH),y)
	GPSD_CONF_OPT += --enable-squelch
endif
ifneq ($(BR2_PACKAGE_GPSD_RECONFIGURE),y)
	GPSD_CONF_OPT += --disable-reconfigure
endif
ifneq ($(BR2_PACKAGE_GPSD_CONTROLSEND),y)
	GPSD_CONF_OPT += --disable-controlsend
endif
ifeq ($(BR2_PACKAGE_GPSD_RAW),y)
	GPSD_CONF_OPT += --enable-raw
endif
ifneq ($(BR2_PACKAGE_GPSD_OLDSTYLE),y)
	GPSD_CONF_OPT += --disable-oldstyle
endif
ifeq ($(BR2_PACKAGE_GPSD_PROFILING),y)
	GPSD_CONF_OPT += --enable-profiling
endif
ifneq ($(BR2_PACKAGE_GPSD_TIMING),y)
	GPSD_CONF_OPT += --disable-timing
endif
ifneq ($(BR2_PACKAGE_GPSD_CLIENT_DEBUG),y)
	GPSD_CONF_OPT += --disable-clientdebug
endif
ifeq ($(BR2_PACKAGE_GPSD_USER),y)
	GPSD_CONF_OPT += --enable-gpsd-user=$(BR2_PACKAGE_GPSD_USER_VALUE)
endif
ifeq ($(BR2_PACKAGE_GPSD_GROUP),y)
	GPSD_CONF_OPT += --enable-gpsd-group=$(BR2_PACKAGE_GPSD_GROUP_VALUE)
endif
ifeq ($(BR2_PACKAGE_GPSD_FIXED_PORT_SPEED),y)
	GPSD_CONF_OPT += --enable-fixed-port-speed=$(BR2_PACKAGE_GPSD_FIXED_PORT_SPEED_VALUE)
endif
ifeq ($(BR2_PACKAGE_GPSD_MAX_CLIENT),y)
	GPSD_CONF_OPT += --enable-max-clients=$(BR2_PACKAGE_GPSD_MAX_CLIENT_VALUE)
endif
ifeq ($(BR2_PACKAGE_GPSD_MAX_DEV),y)
	GPSD_CONF_OPT += --enable-max-devices=$(BR2_PACKAGE_GPSD_MAX_DEV_VALUE)
endif

TARGET_CFLAGS+=-D_GNU_SOURCE -I$(STAGING_DIR)/usr/include/QtCore -I$(STAGING_DIR)/usr/include/QtNetwork




define GPSD_BUILD_CMDS
#	$(SED) 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' $(GPSD_DIR)/libtool
#	$(SED) 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' $(GPSD_DIR)/libtool
#	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) all libgpsmm
	CC="$(TARGET_CC)" CXX="$(TARGET_CXX)" AR="$(TARGET_AR)" STRIP="$(TARGET_STRIP)" LD="$(TARGET_LD)" CFLAGS="$(TARGET_CFLAGS)" LDFLAGS="$(TARGET_LDFLAGS)" LINKFLAGS="$(TARGET_LDFLAGS)" PKG_CONFIG=$(HOST_DIR)/usr/bin/pkg-config $(HOST_DIR)/usr/bin/scons libdir=$(TARGET_DIR)/usr/lib sysroot=$(STAGING_DIR) prefix=$(TARGET_DIR)/usr --jobs=6 python=false $(FEATURES) -C $(@D)
#	PKG_CONFIG_PATH=$(@D)/packaging PKG_CONFIG=$(HOST_DIR)/usr/bin/pkg-config $(HOST_DIR)/usr/bin/scons -target=arm-unknown-linux-gnueabi --sysroot=$(STAGING_DIR) -C $(@D)
endef

define GPSD_INSTALL_TARGET_CMDS
	#$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) DESTDIR=$(TARGET_DIR) install
	#if [ ! -f $(TARGET_DIR)/etc/init.d/S50gpsd ]; then \
		#$(INSTALL) -m 0755 -D package/gpsd/S50gpsd $(TARGET_DIR)/etc/init.d/S50gpsd; \
		#$(SED) 's,^DEVICES=.*,DEVICES=$(BR2_PACKAGE_GPSD_DEVICES),' $(TARGET_DIR)/etc/init.d/S50gpsd; \
	#fi
	
endef

define GPSD_UNINSTALL_TARGET_CMDS
	#rm -f $(addprefix $(TARGET_DIR)/usr/bin/, $(GPSD_TARGET_BINS))
	#rm -f $(TARGET_DIR)/usr/lib/libgps.*
	#rm -f $(TARGET_DIR)/usr/lib/libgpsd.*
	#rm -f $(TARGET_DIR)/usr/sbin/gpsd
	#rm -f $(TARGET_DIR)/etc/init.d/S50gpsd
endef

$(eval $(call GENTARGETS))
