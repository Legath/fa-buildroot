
YAFFS_VERSION=
YAFFS_SOURCE=yaffs2utils.tar.gz
YAFFS_SITE=http://yaffs2utils.googlecode.com/files/

define HOST_YAFFS_BUILD_CMDS
 $(HOST_MAKE_ENV) $(MAKE)  -C $(@D)/src/
endef
define HOST_YAFFS_INSTALL_CMDS
  $(INSTALL)  $(@D)/src/mkyaffs2 $(HOST_DIR)/usr/bin 
endef
$(eval $(call GENTARGETS,host))
