#############################################################
#
# Build the yaffs root filesystem image
#
#############################################################

ifeq ($(BR2_TARGET_ROOTFS_YAFFS),y)
ROOTFS_YAFFS_DEPENDENCIES = host-yaffs
endif


define ROOTFS_YAFFS_CMD
	$(HOST_DIR)/usr/bin/mkyaffs2 $(TARGET_DIR) $(BINARIES_DIR)/rootfs.img
endef

$(eval $(call ROOTFS_TARGET,yaffs))
