#############################################################
#
# diethotplug
#
#############################################################

DIETHOTPLUG_VERSION = 0.4
DIETHOTPLUG_SOURCE = diethotplug-$(DIETHOTPLUG_VERSION).tar.gz
DIETHOTPLUG_SITE = http://www.kernel.org/pub/linux/utils/kernel/hotplug/
DIETHOTPLUG_DEPENDENCIES = linux

define DIETHOTPLUG_BUILD_CMDS
 	$(MAKE) -C $(@D) KERNEL_INCLUDE_DIR=$(LINUX_DIR)/include CROSS=$(BR2_TOOLCHAIN_EXTERNAL_PATH)/bin/$(BR2_TOOLCHAIN_EXTERNAL_CUSTOM_PREFIX)- KLIBC=false INST_MOD_DIR=$(TARGET_DIR)/lib/modules/2.6.38-FriendlyARM all
endef


define DIETHOTPLUG_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/hotplug $(TARGET_DIR)/bin
endef

$(eval $(call GENTARGETS))
